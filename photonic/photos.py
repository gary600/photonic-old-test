from flask import Blueprint, render_template
from . import db, config

bp = Blueprint("photos", __name__)


@bp.route("/collection/<path:path>")
def view_collection(path):
    path_parts = path.split("/")
    collection = db.collections.find_one({"path-id": path_parts[-1]})
    if not collection:
        return "collection not found", 404
    return render_template("photolist.html", collection=collection)


@bp.route("/<image>")
def view_image(image):
    doc = db.images.find_one({"uuid": image})
    if not doc:
        return "image not found", 404


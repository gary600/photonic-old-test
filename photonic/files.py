import os.path
from flask import Blueprint
from . import db, config

bp = Blueprint("files", __name__)


@bp.route("/images/<uuid>/<name>")
def thumb(name, uuid):
    doc = db.images.find_one({"uuid": uuid})
    if not doc:
        return "image not found", 404

    try:
        path = os.path.join(config["images"]["storage-dir"], doc["images"][name]["path"])
        with open(path, "rb") as f:
            img_data = f.read()
    except FileNotFoundError:
        return "image file not found", 500
    except KeyError:
        return "image size not found", 400

    return img_data, 200, {"Content-Type": doc["images"][name]["type"]}
from flask import Flask
from pymongo import MongoClient
import toml

with open("config.toml", "r") as f:
    config = toml.load(f)

app = Flask(__name__)
mongo = MongoClient()
db = mongo.photonic

from .photos import bp as photos
from .files import bp as files


app.register_blueprint(photos, url_prefix="/photos")
app.register_blueprint(files, url_prefix="/files")